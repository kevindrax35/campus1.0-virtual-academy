import React from "react";
import "../styles/Menu.scss";

function Menu() {
  const value = "LogoCampus-01.svg";
  return (
    <aside>
      <div className="avatar"></div>
      <p className="user">Andrea</p>
      <div className="options">
        <a href="javascript:void(0)">
          <i className="fa fa-user-o" aria-hidden="true"></i>
          Inicio
        </a>
        <a href="javascript:void(0)">
          <i className="fa fa-laptop" aria-hidden="true"></i>
          Material
        </a>
        <a href="javascript:void(0)">
          <i className="fa fa-clone" aria-hidden="true"></i>
          Aula virtual
        </a>
        <a href="javascript:void(0)">
          <i className="fa fa-star-o" aria-hidden="true"></i>
          Horarios
        </a>
        <a href="javascript:void(0)">
          <i className="fa fa-trash-o" aria-hidden="true"></i>
          Configuración
        </a>
      </div>
      {/* <img className="Principal-logo" src={value} /> */}
    </aside>
  );
}
export default Menu;
